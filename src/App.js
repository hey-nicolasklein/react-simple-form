import React from 'react';
import logo from './logo.svg';
import './App.css';

class Form extends React.Component{
  constructor(props){
      super(props);
      this.state = {age: 0, tall: 20};

      this.methods = ["handleChange"];

      for(const method of this.methods){
          this[method] = this[method].bind(this)
          
      }
  }


  handleChange(newVal, myID){
      let val = {};
      val[myID] = newVal;
      this.setState(val);
  }

  render(){
      const text = this.state.result;

      return(
          <div class="Main">
              <h3 id="heading">What type of person are you, really?</h3>
              <div class="container">
                <TextInput question= "How old are you?" myID="age"
                onChange={this.handleChange} value={text} />
                <TextInput question= "How tall are you?" myID="tall"
                onChange={this.handleChange} value={text} />
                <TextInput question= "What color are your eyes?" myID="eye"
                onChange={this.handleChange} value={text} />
              </div>
              <p id="result">My age is {this.state.age} and I'm {this.state.tall} tall
                and you have wonderful {this.state.eye} eyes.</p>
          </div>
      )
  }
}

class TextInput extends React.Component{
  constructor(props){
      super(props);
      this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e){
      this.props.onChange(e.target.value, this.props.myID);
  }

  render(){
      const input = this.props.value;
      return (
        <div className="TextInput">
          <form>
              <legend>{this.props.question}</legend>
              <input value={input} onChange={this.handleChange}/>
          </form>
        </div>
      )
  }
}

function App() {
  return (
    <div className="App">
      <Form/>
    </div>
  );
}

export default App;
